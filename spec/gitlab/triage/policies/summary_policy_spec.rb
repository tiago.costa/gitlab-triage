require 'spec_helper'

require 'gitlab/triage/policies/summary_policy'

describe Gitlab::Triage::Policies::SummaryPolicy do
  include_context 'network'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }

  let(:actions) { { summarize: { summary: "This is a summary: {{items}}" } } }
  let(:policy_spec) { { name: name, actions: actions } }
  let(:rule) { { name: 'Rule name' } }

  let(:resources) { { rule => [{ title: name }] } }
  let(:expected_resources) { { rule => [{ title: name, type: type }] } }

  subject { described_class.new(type, policy_spec, resources, network) }

  describe '#resources' do
    it 'returns the resources with type attached' do
      expect(subject.resources).to eq(expected_resources)
    end
  end

  describe '#build_issue' do
    it 'delegates to EntityBuilders::IssueBuilder' do
      # For the wrapping summary issue
      expect(Gitlab::Triage::EntityBuilders::IssueBuilder)
        .to receive(:new)
        .with(
          type: type,
          action: actions[:summarize],
          resources: [],
          network: network)
        .and_call_original

      # For each summary rule
      expect(Gitlab::Triage::EntityBuilders::IssueBuilder)
        .to receive(:new)
        .with(
          type: type,
          action: {},
          resources: expected_resources[rule],
          network: network)
        .and_call_original

      subject.build_issue
    end
  end
end
