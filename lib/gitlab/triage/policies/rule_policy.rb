# frozen_string_literal: true

require_relative 'base_policy'
require_relative '../entity_builders/issue_builder'

module Gitlab
  module Triage
    module Policies
      class RulePolicy < BasePolicy
        # Build an issue from a single rule policy
        def build_issue
          action = actions.fetch(:summarize, {})

          EntityBuilders::IssueBuilder.new(
            type: type,
            action: action,
            resources: resources,
            network: network)
        end
      end
    end
  end
end
